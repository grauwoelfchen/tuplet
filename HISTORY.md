# History

All notable changes belong to releases will be documented in this file.


## Unreleased changes

[tuplet-v0.0.1...master](
https://gitlab.com/n-tuple/tuplet/-/compare/tuplet-v0.0.1...master)


## Tags

### Release v0.0.1

Tag [tuplet-v0.0.1](
https://gitlab.com/n-tuple/tuplet/-/tags/tuplet-v0.0.1) has not been released
yet.

#### Changes

[(initial commit)...tuplet-v0.0.1](
https://gitlab.com/n-tuple/tuplet/-/compare/c8eff6d0...tuplet-v0.0.2)

* Configure CI with `.gitlab-ci.yml`

#### Links

* [crates.io](https://crates.io/crates/tuplet/0.0.1)
* [docs.rs](https://docs.rs/crate/tuplet/0.0.1)
